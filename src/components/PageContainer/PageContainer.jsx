import React, { useState, memo, useEffect } from 'react';
import AddInfo from '../AddInfo/AddInfo';
import './PageContainer.scss';
import shortid from 'shortid'

const PageContainer = (props) => {

  const [data, addNewItem] = useState([]);
  const [addInfo, getAddInfo] = useState([]);
  const [form, setForm] = useState({title:''});
  const [InfoCard, showInfo] = useState(false);


  const changeValue = e => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const closeAdditionalInfo = () => {
    showInfo(false);
  };


  const showAdditionalInfo = e => {
    console.log(e);
    let id = e.target.closest('.card-item').id;

    async function fetchData2() {
      const res = await fetch(`http://www.omdbapi.com/?apikey=4b601aab&i=${id}`);
      const json = await res.json();
      await getAddInfo(json);
      }
      fetchData2();
      showInfo(true);
  };

  useEffect(() =>{

    async function fetchData(form) {
      const res = await fetch(`http://www.omdbapi.com/?apikey=4b601aab&s=${form.title}&y=${form.year}&type=movie&page=1`);
      const json = await res.json();
      await addNewItem(json.Search);
    }
    fetchData(form);
  }, [form]);


  let cardsCollection;
  if (data !== undefined) {
    let newData = data.slice(0,20);
      cardsCollection = newData.map(card => {
      return (
        <div key={shortid()} className="card-item" id={card.imdbID} onClick={showAdditionalInfo}>
          <h3>{card.Title}</h3>
          <p>{card.Year}</p>
          <img alt='movie' src={card.Poster} />
          <a href={card.Poster} download>
            Download image
          </a>
        </div>
      )
    });
  }


  return (
      <main className='main-block'>
        {InfoCard && <div className='overlay' onClick={closeAdditionalInfo}></div>}
        {InfoCard && <AddInfo info={addInfo} closeAdditionalInfo={closeAdditionalInfo}/>}
        <div className="main-block-content">
          <input className='search-input' type="text" placeholder='Title' name='title' onChange={changeValue} value={form.title}/>
          <input className='search-input' type="text" placeholder='Year' name='year' onChange={changeValue} value={form.year}/>
          <div className='card-container'>
            {cardsCollection}
          </div>
        </div>
      </main>
  );
};

export default memo(PageContainer);