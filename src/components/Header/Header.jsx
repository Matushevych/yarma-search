import React from 'react';
import './Header.scss';

const Header = props => {
  return (
    <header className='header'>
      <h1>Searching Movies</h1>
    </header>
  );
};

export default Header;