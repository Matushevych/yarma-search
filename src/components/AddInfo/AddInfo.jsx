import React from 'react';

const AddInfo = props => {
  const {info, closeAdditionalInfo} = props;
  return (
    <div className='additionalInfo' onClick={closeAdditionalInfo}>
      <div className=''>
        <i className="far fa-cross"/>
        <h3>{info.Title}</h3>
        <p>{info.Director}</p>
        <p>{info.Year}</p>
        <p>{info.Released}</p>
        <p>{info.BoxOffice}</p>
        <p>{info.Country}</p>
        <p>{info.Genre}</p>
        <p>{info.Runtime}</p>
        <p>{info.Plot}</p>
        <p>{info.imdbRating}</p>
        <p>{info.imdbVotes}</p>
        <button onClick={closeAdditionalInfo}>Close</button>
      </div>
    </div>
  );
};

export default AddInfo;