import React from 'react';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import PageContainer from './components/PageContainer/PageContainer';
import './App.scss';

const App = (props) => {

  return (
      <>
        <Header/>
        <PageContainer/>
        <Footer/>
      </>
    );
};

export default App;
